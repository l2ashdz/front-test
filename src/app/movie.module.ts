import { NgModule } from '@angular/core';
import { AddMovieComponent } from './components/movie/add-movie/add-movie.component';
import { EditMovieComponent } from './components/movie/edit-movie/edit-movie.component';
import { ListMovieComponent } from './components/movie/list-movie/list-movie.component';
import { SharedModule } from './shared.module';

@NgModule({
  declarations: [
      AddMovieComponent,
      EditMovieComponent,
      ListMovieComponent
  ],
  imports: [
    SharedModule
  ]
})
export class MovieModule { }
