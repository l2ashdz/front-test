import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from 'src/app/model/movie';
import { MovieService } from 'src/app/service/movie.service';

@Component({
    selector: 'app-edit-movie',
    templateUrl: './edit-movie.component.html',
    styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {

    movie: Movie;
    textInfo:string = "";

    constructor(private router: Router, private service: MovieService) {
        this.movie = new Movie();
    }

    ngOnInit(): void {
        this.getMovie();
        this.eventModal();
    }

    getMovie() {
        let idMovie = localStorage.getItem('idMovie') ?? '';
        this.service.getById(idMovie)
        .subscribe(data => {
            this.movie = data;
        });
    }

    onSubmit() {
        this.service.update(this.movie)
        .subscribe(data => {
            this.showInfo('Movie updated!');
        });
    }

    backList() {
        this.router.navigate(['/movie/list']);
    }

    private showInfo(info:string){
        this.textInfo = info;
        document.getElementById("btnModalInfo")?.click();
    }

    private eventModal(){
        document.getElementById('modalInfo')?.addEventListener('hidden.bs.modal', ()=> {
            this.backList();
        });
    }

}
