import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from 'src/app/model/movie';
import { MovieService } from 'src/app/service/movie.service';

@Component({
    selector: 'app-list-movie',
    templateUrl: './list-movie.component.html',
    styleUrls: ['./list-movie.component.css'],
})
export class ListMovieComponent implements OnInit {
    movies: Movie[];
    textInfo: string = '';

    constructor(private router: Router, private service: MovieService) {
        this.movies = new Array();
    }

    ngOnInit(): void {
        this.service.getAll().subscribe((data) => {
            this.movies = data;
        });
    }

    add() {
        this.router.navigate(['/movie/add']);
    }

    editar(movie: Movie) {
        localStorage.setItem('idMovie', movie.movieId.toString());
        this.router.navigate(['/movie/edit']);
    }

    delete(movie: Movie) {
        this.service.delete(movie.movieId).subscribe((data) => {
            this.movies = this.movies.filter((a) => a != movie);
            this.showInfo('Movie deleted');
        });
    }

    private showInfo(info: string) {
        this.textInfo = info;
        document.getElementById('btnModalInfo')?.click();
    }
}
