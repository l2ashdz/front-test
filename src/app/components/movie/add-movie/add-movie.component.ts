import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from 'src/app/model/movie';
import { MovieService } from 'src/app/service/movie.service';

@Component({
    selector: 'app-add-movie',
    templateUrl: './add-movie.component.html',
    styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

    movie: Movie;
    textInfo:string = "";

    constructor(private router: Router, private service: MovieService) {
        this.movie = new Movie();
    }

    ngOnInit(): void {
        this.eventModal();
    }

    onSubmit() {
        this.service.create(this.movie)
        .subscribe(data => {
            this.showInfo('Movie added');
        });
    }

    private eventModal(){
        document.getElementById('modalInfo')?.addEventListener('hidden.bs.modal', ()=> {
            this.backList();
        });
    }

    backList() {
        this.router.navigate(['/movie/list']);
    }

    private showInfo(info:string){
        this.textInfo = info;
        document.getElementById("btnModalInfo")?.click();
    }
}
