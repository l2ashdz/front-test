import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalComponent } from './components/modal/modal.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule],
  exports: [CommonModule, FormsModule, AppRoutingModule, HttpClientModule, ModalComponent],
})
export class SharedModule {}
