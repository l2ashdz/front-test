import { NgModule } from '@angular/core';
import { MovieService } from './service/movie.service';

@NgModule({
    declarations: [],
    providers: [MovieService],
})
export class CoreModule {}
