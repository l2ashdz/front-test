import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MovieModule } from './movie.module';
import { SharedModule } from './shared.module';
import { CoreModule } from './core.module';
import { ManagerRoutesComponent } from './components/manager-routes/manager-routes.component';

@NgModule({
    declarations: [AppComponent, ManagerRoutesComponent],
    imports: [BrowserModule, SharedModule, CoreModule, MovieModule],
    bootstrap: [AppComponent],
})
export class AppModule {}
