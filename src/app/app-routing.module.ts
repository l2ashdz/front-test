import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagerRoutesComponent } from './components/manager-routes/manager-routes.component';
import { AddMovieComponent } from './components/movie/add-movie/add-movie.component';
import { EditMovieComponent } from './components/movie/edit-movie/edit-movie.component';
import { ListMovieComponent } from './components/movie/list-movie/list-movie.component';

const routes: Routes = [
    {
        path: 'movie',
        component: ManagerRoutesComponent,
        children: [
            { path: 'list', component: ListMovieComponent },
            { path: 'add', component: AddMovieComponent },
            { path: 'edit', component: EditMovieComponent },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
