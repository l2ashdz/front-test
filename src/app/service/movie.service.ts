import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Movie } from '../model/movie';

@Injectable({
    providedIn: 'root',
})
export class MovieService {
    private url: String = 'http://localhost:8080/back-test/rest/movies';

    constructor(private http: HttpClient) {}

    create(movie: Movie) {
        return this.http.post<Movie>(`${this.url}`, movie);
    }

    getById(id: string) {
        return this.http.get<Movie>(`${this.url}/${id}`);
    }

    update(movie: Movie) {
        return this.http.put<Movie>(`${this.url}`, movie);
    }

    delete(id: String) {
        return this.http.delete<Movie>(`${this.url}/${id}`);
    }

    getAll() {
        return this.http.get<Movie[]>(`${this.url}`);
    }
}
